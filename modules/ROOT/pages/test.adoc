
= Special Characters and Symbols
// Settings
:idprefix:
:idseparator: -
:table-caption!:

Special character and symbol replacement is built into Asciidoctor.

During conversion, the characters in the table below are replaced with the appropriate character or Unicode entity.
Replacement of special characters and symbols occurs in all inline and block elements except for comments and certain passthroughs.
The three special characters, `<`, `>`, and `&`, are always replaced first.


